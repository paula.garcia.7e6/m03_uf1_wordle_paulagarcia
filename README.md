# WORDLE #
## Funcionament ##
El joc consisteix en adivinar la paraula de 5 lletras, generada aleatoriament amb un diccionari. 

Quant posas una paraula de 5 lletres i que estigui dins del diccionari, indicara amb diferents colors quinas lletres estan y no estan en la paraula que necesitas adivinar. 
* gris: La lletra no esta en la paraula. 
* groc: La lletra esta pero no en la mateixa posició. 
* verd: Esta en la paraula y correctament posicionada. 

## Com s'ha fet ##
El joc t'obliga a posar si o si una paraula de 5 lletres amb un do while. Aquest fa que es repeteixi en bucle fins que posis una paraula valida:  
Escribe una palabra del diccionario de 5 letras  
aaa  
Escribe una palabra del diccionario de 5 letras  
aaaaaa  
Escribe una palabra del diccionario de 5 letras  
junil  
Escribe una palabra del diccionario de 5 letras  
arbol  
[a, r, b, o, l] (Aqui no es veu pero la A y la O estan en groc)  

El joc primer verifica si la paraula és verda, es a dir si és igual que la paraula. Si no és verda verifica si és igual a qualsevol altre lletra de la paraula. S'utilitzen dos for per fer les dues verificacions.  

Finalment tot esta dins d'un do while, aquest do while s'acaba quant la paraula s'acerta o els intents arriban a 0. 